<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['session.login'])->group(function () {
    Route::get('/', function () {
        return view('dashboard.index');
    })->name('dashboard');
    Route::get('/login', 'AuthController@login')->name('login');
    Route::post('/login', 'ConsumeApiController@attemptLogin')->name('post.login');
    Route::get('/logout', 'AuthController@logout')->name('logout');

    Route::get('/ticket/report', 'TicketController@report')->name('ticket.report');
    Route::get('/ticket/beli', 'TicketController@create')->name('ticket.create');
    Route::post('/ticket/beli', 'TicketController@beli')->name('ticket.beli');
    Route::resource('user', 'UserController');
});
