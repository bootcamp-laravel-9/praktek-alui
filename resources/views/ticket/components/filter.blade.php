<form action="/" method="GET" id="filter">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="q">Search</label>
                <input type="text" name="q" id="q" class="form-control" value="{{ request('q') }}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group
            ">
                <label for="category">Kategori</label>
                <select name="category" id="category" class="form-control">
                    <option value="">-- Pilih Kategori --</option>
                    @foreach ($categories as $category)
                        <option value="{{ $category['id'] }}"
                            {{ request('category') == $category['id'] ? 'selected' : '' }}>
                            {{ $category['name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group
        ">
                <label for="start_date">Tanggal Tiket</label>
                <input type="date" name="start_date" id="start_date" class="form-control"
                    value="{{ request('start_date') }}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group
        ">
                <label for="end_date">Sampai</label>
                <input type="date" name="end_date" id="end_date" class="form-control"
                    value="{{ request('end_date') }}" min="{{ request('start_date') }}">
            </div>
        </div>
    </div>
    <div class="mb-2">
        <button type="submit" class="btn btn-primary">Filter</button>
        <a href={{ route('ticket.report') }} class="btn btn-secondary">Reset</a>
    </div>

</form>
