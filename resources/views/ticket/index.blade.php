@extends('layout.main')
@section('menu-ticket', 'active')
@section('menu-ticket-report', 'active')
@section('content')
    <h1 class="h3">Beli tiket</h1>
    {{-- search and filter --}}
    @include('ticket.components.filter', ['categories' => $categories])
    {{-- end search and filter --}}

    <table class="table table-bordered">
        <thead>
            <tr>
                <th style="width: 10px">No</th>
                <th>Nomor Tiket</th>
                <th>
                    Nama
                </th>
                <th>
                    Email
                </th>
                <th>
                    No Telpon
                </th>
                <th>
                    Kategori
                </th>

                <th>
                    Tgl Tiket
                </th>

                <th>Total</th>

            </tr>
        </thead>
        <tbody>
            @if ($data->isEmpty())
                <tr>
                    <td colspan="8" class="text-center">Data tidak ditemukan</td>
                </tr>
            @else
                @foreach ($data as $value)
                    <tr>
                        <td>{{ $loop->iteration }}.</td>
                        <td>{{ $value->no_tiket }}</td>
                        <td>{{ $value->nama }}</td>
                        <td>{{ $value->email }}</td>
                        <td>{{ $value->no_telp }}</td>
                        <td>{{ $value->category_name }}</td>
                        <td>{{ $value->date_ticket }}</td>
                        <td>{{ $value->total_ticket }}</td>
                    </tr>
                @endforeach
            @endif

        </tbody>
    </table>
    {{ $data->appends([
            'q' => request('q'),
            'category' => request('category'),
            'start_date' => request('start_date'),
            'end_date' => request('end_date'),
        ])->links() }}

    <script>
        const formFilter = document.getElementById('filter');
        const search = document.getElementById('search');
        const category = document.getElementById('category');
        const startDate = document.getElementById('start_date');
        const endDate = document.getElementById('end_date');

        const url = new URL(window.location);
        const searchParams = url.searchParams;


        startDate.addEventListener('change', function() {
            if (startDate.value) {
                endDate.min = startDate.value
            } else {
                endDate.min = '';
            }
        });

        formFilter.addEventListener('submit', function(e) {
            e.preventDefault();
            const searchValue = q.value
            const categoryValue = category.value
            const startDateValue = startDate.value;
            const endDateValue = endDate.value;

            const isEmpty = !searchValue && !categoryValue && !startDateValue && !endDateValue;

            if (isEmpty) {
                if (url.search) {
                    window.location = url.pathname
                }
                return
            }


            searchParams.set('page', 1);

            if (searchValue) {
                searchParams.set('q', searchValue);
            } else {
                searchParams.delete('q');
            }

            if (categoryValue) {
                searchParams.set('category', categoryValue);
            } else {
                searchParams.delete('category');
            }

            if (startDateValue) {
                searchParams.set('start_date', startDateValue);
            } else {
                searchParams.delete('start_date');
            }

            if (endDateValue) {
                searchParams.set('end_date', endDateValue);
            } else {
                searchParams.delete('end_date');
            }
            window.location = url.toString();
        });
    </script>
@endsection
