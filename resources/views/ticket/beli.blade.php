@extends('layout.main')
@section('menu-ticket', 'active')
@section('menu-ticket-beli', 'active')
@section('content')
    <h1 class="h3">Tambah tiket</h1>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul class="mb-0">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form id="form-beli">
        @csrf
        <div class="form-group">
            <label for="no_tiket">Nomor Tiket</label>
            <input type="text" name="no_tiket" id="no_tiket" class="form-control" {{-- value is old value or random generated --}}
                value="{{ old('no_tiket', 'T' . rand(1000, 9999)) }}" readonly>
        </div>
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" name="nama" id="nama" class="form-control" value="{{ old('nama') }}">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}">
        </div>
        <div class="form-group">
            <label for="address">Address</label>
            <textarea name="address" id="address" class="form-control" value="{{ old('address') }}" rows="3"></textarea>
        </div>
        <div class="form-group">
            <label for="no_telp">No Telpon</label>
            <input type="text" name="no_telp" id="no_telp" class="form-control" value="{{ old('no_telp') }}">
        </div>
        <div class="form-group">
            <label for="category">Kategori</label>
            <select name="ticket_category" id="category" class="form-control">
                <option value="">-- Pilih Kategori --</option>
                @foreach ($categories as $category)
                    <option value="{{ $category->id }}" {{ old('category') == $category->id ? 'selected' : '' }}>
                        {{ $category->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="date-picker">Tanggal Tiket</label>
            <input type="date" name="date-picker" id="date-picker" class="form-control" value="{{ old('date-picker') }}">
            <input type="hidden" name="date_ticket" id="date_ticket" value="{{ old('date_ticket') }}">
        </div>

        <div class="form-group">
            <label for="total_ticket">Total</label>
            <input type="number" name="total_ticket" id="total_ticket" class="form-control" min="1"
                value="{{ old('total_ticket') }}">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </form>

    <script>
        // make date picker if selected, the type will change to text

        const datePicker = document.getElementById('date-picker');
        const dateTicket = document.getElementById('date_ticket');
        datePicker.min = new Date().toISOString().split('T')[0];

        // change date input based on type
        ['change', 'click', 'blur'].forEach(event => {
            datePicker.addEventListener(event, function() {
                if (datePicker.type === 'date' && datePicker.value && event === 'change') {
                    datePicker.type = 'text';
                    datePicker.value = new Date(datePicker.value).toLocaleDateString('id-ID', {
                        day: 'numeric',
                        month: 'long',
                        year: 'numeric'
                    });


                    dateTicket.value = new Date(datePicker.value).toISOString().split('T')[0];
                    return;
                } else if (datePicker.type === 'text' && event === 'click') {
                    datePicker.type = 'date';
                    datePicker.showPicker();
                    return;
                }
            });
        });
        const noTiket = document.getElementById('no_tiket');
        noTiket.value = `T${new Date().getTime()}`;

        function postTicket() {
            const noTiket = document.getElementById('no_tiket').value;
            const nama = document.getElementById('nama').value;
            const email = document.getElementById('email').value;
            const noTelp = document.getElementById('no_telp').value;
            const address = document.getElementById('address').value;
            const dateTicket = document.getElementById('date_ticket').value;
            const category = document.getElementById('category').value;
            const totalTicket = document.getElementById('total_ticket').value;

            const url = "{{ route('ticket.beli') }}";
            const data = {
                no_tiket: noTiket,
                nama: nama,
                email: email,
                no_telp: noTelp,
                address: address,
                date_ticket: dateTicket,
                ticket_category: category,
                total_ticket: totalTicket
            };


            fetch(url, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content
                    },
                    body: JSON.stringify(data)
                })
                .then(response => response.json())
                .then(data => {
                    console.log('Success:', data);
                    toastr.success('Data berhasil disimpan');
                    // clear form
                    document.getElementById('form-beli').reset();
                })
                .catch((error) => {
                    console.error('Error:', error);
                    toastr.error('Data gagal disimpan');
                });
        }

        document.getElementById('form-beli').addEventListener('submit', function(e) {
            e.preventDefault();

            postTicket();
        });
    </script>
@endsection
