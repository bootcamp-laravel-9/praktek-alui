<aside class="main-sidebar sidebar-dark">
    <!-- Brand Logo -->
    <a href="/" class="brand-link text-dark">
        <span class="brand-text font-weight-light text-dark">Berijalan</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('AdminLTE') }}/dist/img/user2-160x160.jpg" class="img-circle elevation-2"
                    alt="User Image">
            </div>
            <div class="info">
                <p class="d-block text-black">
                    Novianto
                </p>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('dashboard') }}" class="nav-link @yield('menu-dashboard')">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href={{ route('user.index') }} class="nav-link @yield('menu-user-management')">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            User Management
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="#" class="nav-link @yield('menu-ticket')">
                        <i class="nav-icon fas fa-ticket-alt"></i>
                        <p>
                            Ticket
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('ticket.report') }}" class="nav-link @yield('menu-ticket-report')">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Report Ticket</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href={{ route('ticket.create') }} class="nav-link @yield('menu-ticket-beli')">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Beli Ticket</p>
                            </a>
                        </li>
                    </ul>
                </li>

                {{-- <li class="nav-item">
                    <a href="{{ route('biodata') }}" class="nav-link @yield('menu-biodata')">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Curiculum Vitae
                        </p>
                    </a>
                </li> --}}
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
