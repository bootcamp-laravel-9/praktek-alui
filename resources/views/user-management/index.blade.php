@extends('layout.main')
@section('menu-user-management', 'active')
@section('content')
    <div class="d-flex justify-content-between align-items-center">
        <h1>
            Master Admin
        </h1>
        <a href="{{ route('user.create') }}" class="btn btn-primary">
            Tambah
        </a>
    </div>


    <table class="table table-bordered">
        <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>


            @foreach ($data as $value)
                <tr>
                    <td>{{ $loop->iteration }}.</td>
                    <td>{{ $value['name'] }}</td>

                    <td>{{ $value['email'] }}</td>
                    <td>

                        <a href="{{ route('user.edit', $value['id']) }}" class="btn btn-sm btn-warning">Edit</a>
                        <button type="button" class="btn btn-sm btn-danger" data-bs-toggle="modal"
                            data-id="{{ $value['id'] }}" data-bs-target="#modaldelete">
                            Delete
                        </button>
                    </td>
                </tr>
            @endforeach

        </tbody>
    </table>

    {{ $data->links() }}

    <div class="modal" tabindex="-1" id="modaldelete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p id="modal-text"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <form action="/" method="POST" style="display: inline" id="form-delete">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        const modaldelete = document.getElementById('modaldelete');
        const modaltext = document.getElementById('modal-text');
        const btnDelete = document.querySelectorAll('.btn-danger');
        const formDelete = document.getElementById('form-delete');

        btnDelete.forEach((btn) => {
            btn.addEventListener('click', function() {
                const id = this.getAttribute('data-id');
                console.log(id);

                modaltext.innerHTML = `Apakah anda yakin ingin menghapus data ini?`;
                // "{{ route('user.destroy', $value['id']) }}"
                formDelete.action = "{{ route('user.destroy', $value['id']) }}"
            });
        });
    </script>
@endsection
