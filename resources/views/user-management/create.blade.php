@extends('layout.main')
@section('menu-user-management', 'active')
@section('content')
    <div class="container">
        <h1>
            Tambah User
        </h1>
        {{-- @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>
                            {{ $error }}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif --}}
        <form action="{{ route('user.store') }}" method="POST">
            @csrf
            <div class="">
                <div class="form-group">
                    <label for="name">
                        Nama
                    </label>
                    <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                        id="name" placeholder="Enter nama" value="{{ old('name') }}">

                    @error('name')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email"
                        class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" id="email"
                        placeholder="email" value="{{ old('email') }}">
                    @error('email')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password"
                        class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" id="password"
                        placeholder="password">
                    @error('password')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="confirmpassowd">Confirm Password</label>
                    <input type="password" name="confirmpassword"
                        class="form-control {{ $errors->has('confirmpassword') ? 'is-invalid' : '' }}" id="confirmpassword"
                        placeholder="confirm password">
                    @error('confirmpassword')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

            </div>
            <!-- /.card-body -->
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="{{ route('user.index') }}" class="btn btn-secondary">Cancel</a>
        </form>
    </div>
@endsection
