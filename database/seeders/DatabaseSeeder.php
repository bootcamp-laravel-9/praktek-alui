<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();
        // \App\Models\TicketCategory::factory(10)->create();
        // \App\Models\TicketHeader::factory(10)->create();
        // \App\Models\TicketDetail::factory(10)->create();
    }
}
