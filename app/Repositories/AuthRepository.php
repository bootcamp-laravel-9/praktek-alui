<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthRepository
{
    public function login($request)
    {
        $credentials = $request->only('email', 'password');
        if (!Auth::attempt($credentials)) {
            throw new \Exception('Unauthorized', 401);
        }

        $user = User::where('email', $credentials['email'])->first();


        $user['token'] = $user->createToken(config('app.name'))->plainTextToken;

        return $user;
    }
}
