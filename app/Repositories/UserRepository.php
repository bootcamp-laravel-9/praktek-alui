<?php

namespace App\Repositories;

use App\Models\User;


class UserRepository
{
    public function all()
    {
        // paginate user
        return User::paginate(10);
    }

    public function create($data)
    {
        return User::create($data);
    }

    public function update($data, $id)
    {
        if (!isset($data['password']) || empty($data['password'])) {
            unset($data['password']);
        }
        $user = User::find($id);
        if (!$user) {
            throw new \Exception('User not found');
        } else {
            $user->update($data);
        }
    }

    public function delete($id)
    {
        $user = User::find($id);
        if (!$user) {
            throw new \Exception('User not found');
        } else {
            $user->delete();
        }
    }
}
