<?php

namespace App\Repositories;

use App\Models\TicketCategory;
use App\Models\TicketDetail;
use App\Models\TicketHeader;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TicketRepository
{
    public function getCategory()
    {
        return TicketCategory::all();
    }

    public function filter($request)
    {
        $q = strtolower($request->query('q'));
        $category = $request->query('category');
        $startDate = $request->query('start_date');
        $endDate = $request->query('end_date');

        // if ($startDate && !isset($endDate)) {
        //     toastr()->error('End Date is required');
        //     // redirect to /ticket/report
        //     // return object
        //     return redirect()->route('ticket.report');
        //     // return redirect()->back();
        // }

        // if ($endDate && !isset($startDate)) {
        //     toastr()->error('Start Date is required');
        //     return redirect()->route('ticket.report');
        // }

        $data = DB::table('ticket_headers')
            ->join('ticket_details', 'ticket_headers.id', '=', 'ticket_details.ticket_header_id')
            ->join('ticket_categories', 'ticket_details.ticket_category', '=', 'ticket_categories.id')
            ->select('ticket_headers.*', 'ticket_details.*', 'ticket_categories.name as category_name');

        if ($q) {
            // search by no tiket, nama, email, no telp, address, category name
            $data->where(function ($query) use ($q) {
                $query->where('ticket_headers.no_tiket', 'like', '%' . $q . '%')
                    ->orWhere('ticket_headers.nama', 'like', '%' . $q . '%')
                    ->orWhere('ticket_headers.email', 'like', '%' . $q . '%')
                    ->orWhere('ticket_headers.no_telp', 'like', '%' . $q . '%')
                    ->orWhere('ticket_headers.address', 'like', '%' . $q . '%')
                    ->orWhere('ticket_categories.name', 'like', '%' . $q . '%');
            });
        }

        if ($category) {
            $data->where('ticket_categories.id', $category);
        }

        if ($startDate && !$endDate) {
            $data->where('ticket_headers.date_ticket', '>=', $startDate);
        }

        if (!$startDate && $endDate) {
            $data->where('ticket_headers.date_ticket', '<=', $endDate);
        }

        if ($startDate && $endDate) {
            $data->whereBetween('ticket_headers.date_ticket', [$startDate, $endDate]);
        }

        // paginate
        $result = $data->paginate(10)->appends(request()->query());
        return $result;
    }

    public function beli($request)
    {
        $validated = $request->validated();

        // save data to database
        $ticketHeader = TicketHeader::create([
            'no_tiket' => $validated['no_tiket'],
            'nama' => $validated['nama'],
            'email' => $validated['email'],
            'no_telp' => $validated['no_telp'],
            'address' => $validated['address'],
            'date_ticket' => $validated['date_ticket']
        ]);

        TicketDetail::create([
            'ticket_header_id' => $ticketHeader->id,
            'ticket_category' => $validated['ticket_category'],
            'total_ticket' => $validated['total_ticket']
        ]);

        return response()->json([
            'message' => 'success',
            'data' => $ticketHeader
        ]);
    }
}
