<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $data = $this->userRepository->all();
        return view('user-management.index', compact('data'));
    }

    public function create()
    {
        return view('user-management.create');
    }

    public function store(UserRequest $request)
    {
        $validated = $request->validated();
        $validated['password'] = bcrypt($validated['password']);
        $this->userRepository->create($validated);
        return redirect()->route('user.index')->with('success', 'User created successfully');
    }

    public function edit($id)
    {
        $data = User::find($id);
        return view('user-management.edit', compact('data'));
    }

    public function update(UserRequest $request, $id)
    {
        $validated = $request->validated();
        if (isset($validated['password'])) {
            $validated['password'] = bcrypt($validated['password']);
        }
        $this->userRepository->update($validated, $id);
        return redirect()->route('user.index')->with('success', 'User updated successfully');
    }

    public function destroy($id)
    {
        try {
            $this->userRepository->delete($id);
            return redirect()->route('user.index')->with('success', 'User deleted successfully');
        } catch (\Exception $e) {
            return redirect()->route('user.index')->with('error', $e->getMessage());
        }
    }
}
