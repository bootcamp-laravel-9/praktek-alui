<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use App\Models\User;
use App\Repositories\AuthRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    private AuthRepository $authRepository;

    public function __construct(AuthRepository $authRepository)
    {
        $this->authRepository = $authRepository;
    }

    public function login()
    {
        return view('auth.login');
    }

    public function postLogin(AuthRequest $request)
    {

        try {
            $user =  $this->authRepository->login($request);
            return response()->json([
                'success' => true,
                'message' => 'Login success',
                'data' => $user
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function logout(Request $request)
    {
        $request->session()->forget('LoginSession');
        $request->session()->flush();
        toastr()->success('Logout Success');
        return redirect()->route('login');
    }
}
