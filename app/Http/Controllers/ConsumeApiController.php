<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConsumeApiController extends Controller
{
    public function attemptLogin()
    {
        $request = Request::create(url('api/login'), 'POST');
        $response = app()->make('router')->dispatch($request);
        $authData = json_decode($response->getContent(), true);
        if ($authData && $authData['success']) {
            session(['LoginSession' => $authData['data']]);
            toastr()->success('Login Success');
            return redirect()->route('dashboard');
        } else {
            toastr()->error('Login Failed');
            return redirect()->route('login')->withInput();
        }
    }

    // public function getReport()
    // {
    //     $request = Request::create(url('api/report'), 'GET');
    //     $response = app()->make('router')->dispatch($request);
    //     $data = json_decode($response->getContent(), true);
    //     // dd($data['data']['data']);
    //     return view('ticket.index', [
    //         'data' => $data['data']['data'],
    //         'categories' => $data['categories']
    //     ]);
    // }
}
