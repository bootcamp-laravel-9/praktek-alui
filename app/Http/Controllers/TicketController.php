<?php

namespace App\Http\Controllers;

use App\Http\Requests\BeliTicketRequest;
use App\Models\TicketDetail;
use App\Models\TicketHeader;
use App\Repositories\TicketRepository;
use Illuminate\Http\Request;

class TicketController extends Controller
{

    private TicketRepository $ticketRepository;

    public function __construct(TicketRepository $ticketRepository)
    {
        $this->ticketRepository = $ticketRepository;
    }

    public function report()
    {

        $data = $this->ticketRepository->filter(request());
        $category = $this->ticketRepository->getCategory();

        return view('ticket.index', [
            'data' => $data,
            'categories' => $category
        ]);
    }

    public function beli(BeliTicketRequest $request)
    {
        $validated = $request->validated();
        if (!$validated) {
            return response()->json([
                'message' => 'error',
                'data' => $validated
            ]);
        }
        // save data to database
        $ticketHeader = TicketHeader::create([
            'no_tiket' => $validated['no_tiket'],
            'nama' => $validated['nama'],
            'email' => $validated['email'],
            'no_telp' => $validated['no_telp'],
            'address' => $validated['address'],
            'date_ticket' => $validated['date_ticket']
        ]);

        TicketDetail::create([
            'ticket_header_id' => $ticketHeader->id,
            'ticket_category' => $validated['ticket_category'],
            'total_ticket' => $validated['total_ticket']
        ]);

        return response()->json([
            'message' => 'success',
            'data' => $ticketHeader
        ]);
    }

    public function create()
    {
        $category = $this->ticketRepository->getCategory();
        return view('ticket.beli', [
            'categories' => $category
        ]);
    }
}
