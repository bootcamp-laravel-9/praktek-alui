<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BeliTicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {

        return [
            'no_tiket' => 'required',
            'nama' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => '',
            'no_telp' => '',
            'address' => 'required',
            'date_ticket' => 'required',
            'total_ticket' => 'required|numeric',
            'ticket_category' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nama.regex' => 'Nama tidak boleh ada angka'
        ];
    }
}
