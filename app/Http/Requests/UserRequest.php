<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [];
        if ($this->isMethod('post')) {
            $rules = [
                'name' => 'required|regex:/^[\pL\s\-]+$/u',
                'email' => 'required|email|unique:users,email',
                'password' => 'required',
                'confirmpassword' => 'required|same:password',
            ];
        } else if ($this->isMethod('put')) {
            $rules = [
                'name' => 'required|regex:/^[\pL\s\-]+$/u',
                'email' => 'required|email|unique:users,email,' . $this->user,
                'password' => '',
                'confirmpassword' => 'same:password',
            ];
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Nama harus diisi',
            'name.regex' => 'Nama hanya boleh berisi huruf',
            'email.required' => 'Email harus diisi',
            'email.email' => 'Email tidak valid',
            'password.required' => 'Password harus diisi',
            'confirmpassword.required' => 'Konfirmasi password harus diisi',
            'confirmpassword.same' => 'Konfirmasi password tidak sama dengan password',
        ];
    }
}
