<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $path = $request->path();
        $authPath = ['login', 'register'];

        $hasSession = $request->session()->has('LoginSession');

        // dd($hasSession);

        $isAuthPath = in_array($path, $authPath);
        if (!$hasSession && !$isAuthPath) {
            return redirect()->route('login');
        }

        if ($hasSession && $isAuthPath) {
            return redirect()->route('dashboard');
        }

        return $next($request);
    }
}
